/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Joonas
 */
public class Timo extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        stage.setTitle("TIMO-järjestelmä");
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * @param args the command line arguments
     */
    
    private final SpControl spControl = SpControl.getInstance();
    private final Storage storage = Storage.getInstance();
    private static final Log log = Log.getInstance();
    
    public static void main(String[] args) {
        launch(args);
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                log.writeLogFile();
            }
        }, "Shutdown-thread"));
    }

    public Storage getStorage() {
        return storage;
    }
    
    public SpControl getSpControl() {
        return spControl;
    }
    
    public Log getLog() {
        return log;
    }
}
