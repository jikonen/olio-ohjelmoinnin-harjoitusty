/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Joonas
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private ComboBox<Smartpost> comboSP;
    @FXML
    private ComboBox<String> cityCombo;
    @FXML
    private WebView webView;
    @FXML
    private Button addMarkButton;
    @FXML
    private Button clearMapButton;
    @FXML
    private Button createPacketButton;
    @FXML
    private TextArea textArea;
    @FXML
    private ComboBox<Package> packageCombo;
    @FXML
    private Button sendPackageButton;
    
    private Timo timo = new Timo();
    private Storage store = timo.getStorage();
    private SpControl spControl = timo.getSpControl();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        ArrayList<Smartpost> ar = spControl.getSpArray();
        fillComboAll(ar);
        
        // Add cities with smposts to cityCombo
        cityCombo.getItems().add("Kaikki automaatit"); 
        for(Smartpost sp2 : ar) {
            if(!cityCombo.getItems().contains(sp2.getCity())) 
                cityCombo.getItems().add(sp2.getCity());
        }
        
        // Listener for changes to city choice and add posts of city to combo
        cityCombo.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String city) {
                if (city.equals("Kaikki automaatit")) {
                    fillComboAll(ar);
                } else {
                    comboSP.getItems().clear();
                    Smartpost sp2 = new Smartpost("s","s","a","a","Lisää kaikki automaatit","1.11","1.11");
                    comboSP.getItems().add(sp2);
                    for(Smartpost sp : spControl.getSpArrayOfCity(city)) {
                        comboSP.getItems().add(sp);
                    }
                }
            }
        });
    }    

        // Fill Smartpost combo with whole list
    private void fillComboAll(ArrayList<Smartpost> sparray) {
        Smartpost sp2 = new Smartpost("s","s","a","a","Lisää kaikki automaatit","1.11","1.11");
        comboSP.getItems().add(sp2);
        for(Smartpost sp : sparray) {
            comboSP.getItems().add(sp);
        }
    }
  
    @FXML
    private void addMarkerAction(ActionEvent event) {
        // Add all towns markers to map if first sp chosen
        if (comboSP.getValue() == null) {
            textArea.setText("Valitse lisättävä(t)\n" + textArea.getText());
        } else {
            if (comboSP.getValue().getLocName().equals("Lisää kaikki automaatit")) {
                for(Smartpost sp : comboSP.getItems()) {
                    // Check for first fake sp
                    if(sp.getLocName().equals("Lisää kaikki automaatit")) {
                        continue;
                    }

                    String post = sp.getLocName().split(", ")[1];
                    String str = "'" + sp.getAddress()
                        + ", " + sp.getPostCode()
                        + " " + sp.getCity() + "', '" + post + ", Auki: " 
                        + sp.getAvailable() + "', 'blue'";
        //        System.out.println(str);
                webView.getEngine().executeScript("document.goToLocation("+str+")");
                }
            } else {
                String post = comboSP.getValue().getLocName().split(", ")[1];
                String str = "'" + comboSP.getValue().getAddress()
                        + ", " + comboSP.getValue().getPostCode()
                        + " " + comboSP.getValue().getCity() + "', '" + post + ", Auki: " 
                        + comboSP.getValue().getAvailable() + "', 'blue'";
        //        System.out.println(str);
                webView.getEngine().executeScript("document.goToLocation("+str+")");
                }
        }
    }


    @FXML   // Removes all drawn paths from map
    private void clearMapAction(ActionEvent event) {
        webView.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void openPacketCreation(ActionEvent event) {
        
        try {
            Stage stage = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLCreatePacket.fxml"));
            
            Scene scene = new Scene(page);
            stage.setTitle("Luo/Seuraa paketteja");
            stage.setScene(scene);
            stage.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML // Draw package route on map and check for max distance if 1. class
    private void sendPackage(ActionEvent event) {
        Package pk = packageCombo.getValue();
        if (pk == null) {
            textArea.setText("Valitse lähetettävä paketti.\n" + textArea.getText());
        } else {
            Smartpost startSp = pk.getStart();
            Smartpost targetSp = pk.getTarget();
            ArrayList<Float> coordArray = new ArrayList<>();
            coordArray.add(startSp.getLat());
            coordArray.add(startSp.getLng());
            coordArray.add(targetSp.getLat());
            coordArray.add(targetSp.getLng());
            String color = "red";
            int cl = Integer.parseInt(pk.getClas().substring(0, 1));
            double distance = Double.parseDouble(String.valueOf(webView.getEngine().executeScript(
                    "document.createPath(" + coordArray + ", '"+color+"', "+cl+")")));
            
            // Remove sent packet from packet array and combo
            store.getPackageList().remove(pk);
            packageCombo.getItems().remove(pk);
            
            // Log
            ArrayList<String> logAr = new ArrayList<>();
            logAr.add(String.valueOf(store.getPackageList().size()));
            logAr.add(pk.getStart().getCity() + ", " + 
                    pk.getStart().getLocName().split(", ")[1]);
            logAr.add(pk.getTarget().getCity() + ", " + 
                    pk.getTarget().getLocName().split(", ")[1]);
            logAr.add(String.valueOf(distance));
            logAr.add(pk.getItem().getName());
            logAr.add(pk.getClas().substring(0, 1));
            
            
            // Check for distance if 1. class
            if(cl == 1 && ((FirstClass)pk).getMaxDistance() < distance) {
                // Possible future datagathering here
                String str = "TIMO-mies totesi että matka oli\n"
                        + " pitkä ja pysähtyi lepäämään, \n"
                        + "herättyään ei hän pakettiä \n"
                        + "löytänytkään enää mistään. \n"
                        + "'Voi, voi.' Sanoi TIMO-mies ja \n"
                        + "käänsi kylkeään.";
                textArea.setText(str + "\n" + textArea.getText());
                logAr.add("TIMO-mies väsähti ja toimitus ei löytänyt perille.");
            }
            // Break and throw, make comment on index 6 or add to already made
            if (handleBreaking(pk)) {
                if (logAr.size()<7) {
                    logAr.add("Paketti hajosi.");
                } else if (logAr.size() == 7)
                    logAr.set(6, "Paketti hajosi." + "\n" + logAr.get(6));
            }
            if (logAr.size() < 7) {
                logAr.add("Paketti toimitettu normaalisti.");
                textArea.setText("Paketti toimitettu normaalisti.\n" + textArea.getText());
            }
            timo.getLog().entry(logAr);
        }
    }
 
    private boolean handleBreaking(Package pk) {
        boolean isBroken;
        int chanceToBreak = pk.getItem().getChanceToBreak();
        // if chance to break and class 1 then break
        if (Integer.parseInt(pk.getClas().substring(0, 1)) == 1 && chanceToBreak == 1) {
            textArea.setText("Esine hajosi.\n" + textArea.getText());
            isBroken = true;
        }
        // if chance to break and class 3 then calculate number of throws
        // each throw has 1/(number of throws + 10) chance to break
        else if (Integer.parseInt(pk.getClas().substring(0, 1)) == 3
                && chanceToBreak == 1) {
            double numberOfThrows = ((ThirdClass)pk).getThrowFreq();
            //System.out.println(numberOfThrows);
            isBroken = pk.getItem().throwItem(textArea, numberOfThrows);
        }
        // if neither of the above item wont break
        else isBroken = false;
        return isBroken;
    }

    @FXML // Update ComboBox list of packages
    private void updatePackages(MouseEvent event) {
        packageCombo.getItems().clear();
        for(Package pk : store.getPackageList()){
            packageCombo.getItems().add(pk);
        }
    }
}
