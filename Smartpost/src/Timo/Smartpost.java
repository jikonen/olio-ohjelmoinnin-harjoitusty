/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;

/**
 *
 * @author Joonas
 */
public class Smartpost {
    //Location and availability of smartpost
    private final String postCode;
    private final String city;
    private final String address;
    private final String available;
    private String locName;

    
    private final float lat;
    private final float lng;
    
    public Smartpost(String i, String c, String a, String t, String n, 
            String la, String ln){
        postCode = i;
        city = c;
        address = a;
        available = t;
        locName = n;
        lat = Float.valueOf(la);
        lng = Float.valueOf(ln);
//        System.out.println(city);
    }
    public void setLocName(String locName) {
        this.locName = locName;
    }
    
    public String getAddress() {
        return address;
    }

    public String getAvailable() {
        return available;
    }

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }
    
    public String getLocName() {
        return locName;
    }
    
    public String getCity() {
        return city;
    }

    public String getPostCode() {
        return postCode;
    }
    
//To show smartpost location in combobox listing
    @Override
    public String toString(){
        return locName;
    }   

}
