/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;


import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.TextArea;

/**
 *
 * @author Joonas
 */
public class Log {
    private static Log log = new Log();
    
    private ArrayList<String> entry;
    private ArrayList<ArrayList> info;
    private int entryCounter = 0;
    // Time last refresh
    private Date d = new Date();
    private SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
    
    private Log() {
        entry = new ArrayList<>();
        info = new ArrayList<>();
    }
    
    public void entry(ArrayList<String> ar) {
        // Takes ArrayList with Strings: amount of pcks [0], start[1], target[2],
        // length[3], item[4], class[5], notes[6]
        // Start and target as "city, stripped locName"
        entry = ar;
        info.add(0, ar);
        entryCounter++;
    }
    
    public String writeInfoTab(TextArea ta) {
        // Check if there are any entries and if there are any packets sent
        if (!info.isEmpty()) {
            entry = info.get(0);
            if (entry.size() > 5) {
                // Lets test iterator
                ListIterator li = info.listIterator(info.size());
                while(li.hasPrevious()) {
                    ArrayList<String> e = (ArrayList<String>)li.previous();
                    String str = "Paketteja varastossa: " + e.get(0) + "\n" + 
                            "Toimitukset: \n" + e.get(1) + "\n   -> " + e.get(2)
                            + "\nEtäisyys: " + e.get(3) + "\n" + 
                            "Sisältö: " + e.get(4) + "\n" + "Paketin luokka: " + 
                            e.get(5) + "\nHuomioitavaa: \n" + e.get(6) + "\n";
                    ta.setText(str + "\n" + ta.getText());
                }
            } else {
                ta.setText("Paketteja varastossa: " + entry.get(0) + "\n"
                        + "Lähetyksiä ei ole tehty.");
            } 
        } else {
            ta.setText("Loki on tyhjä.");
        }
        return ft.format(d);
    }
    
    public void writeLogFile() {
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream("timoLogFile.txt"), "utf-8"));
            writer.write("TIMO-järjestelmän loki tiedosto.    " + ft.format(d) + " \n\n"
                    + "Paketteja jäi varastoon: " + info.get(0).get(0) + "\n\n"
                    + "Toimitetut paketit: \n\n");
            ListIterator li = info.listIterator(info.size());
                while(li.hasPrevious()) {
                entry = (ArrayList<String>)li.previous();
                writer.append(entry.get(1) + "\n     -> " + entry.get(2) + "\n" +
                         "Etäisyys: " + entry.get(3) + "km, Sisältö: " +
                        entry.get(4) + "\n" + "Paketin luokka: " + entry.get(5) 
                        + "\nHuomioitavaa: " + entry.get(6) + "\n\n");
                }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        } finally {try {
            writer.close();} catch (IOException ex) {
                Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
            }
}
    }
    // IF empty return null;
    public ArrayList<String> getEntryAr() {
        if (info.size() == 0) 
            return null;
        else {
            entry = info.get(0);
            return entry;
        }
    }
    
    public void updateLatest(ArrayList<String> ar) {
        info.set(0, ar);   
    }
    
    public static Log getInstance() {
        return log;
    }
    
    
    
    
}
