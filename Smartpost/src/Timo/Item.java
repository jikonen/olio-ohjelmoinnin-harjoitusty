/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;

import javafx.scene.control.TextArea;

/**
 *
 * @author Joonas
 */


class Letter extends Item {
    
    public Letter() {
        super(2, 0.001, 0, "Kirje");
    }
}

class Cake extends Item {
    public Cake() {
        super(2000, 1, 1, "Kakku");
    }
}

class UnknownBrownMass extends Item {
    public UnknownBrownMass() {
        super(60, 0.4, 0, "Epämääräinen ruskea massa");
    }
    
    public String smells() {
        return "TIMO-mies ei tykkää hajusta";
    }
}

class Ied extends Item {
    public Ied() {
        super(150, 2, 1, "IED");
    }
    @Override
    public boolean throwItem(TextArea textArea, double n) {
        // Timo explodes with 20% chance if broken
        boolean state = false;
        for(int i = 0; i<n; i++) {
            state = Math.random() < 0.001;
            if (state)
                break;
        }
        
        if (state == true && Math.random() > 0.80) {
            textArea.setText("Esine hajosi, TIMO-mies räjähti.\n" 
                    + textArea.getText());
        } else if (state == true) {
           textArea.setText("Esine hajosi.\n" + textArea.getText());
       }
        return state;    
    }
}

class Anvil extends Item {
        
    public Anvil() {
        super(300000, 499, 1, "Alasin");
    }
    
    @Override
    public boolean throwItem(TextArea tArea, double n) {
        // 80% Chance to break Timo if not broken
        boolean state = false;
        for(int i = 0; i<n; i++) {
            state = Math.random() < 0.001;
            if (state)
                break;
        }
        if (state == false && Math.random() > 0.8) {
            tArea.setText("TIMO-mies rikkoi selkänsä.\n" + tArea.getText());
        } else if (state == true) {
           tArea.setText("Esine hajosi.\n" + tArea.getText());
       }
        
        return state;
    }
}

public class Item {
    private final double size;    //cm^3
    private final double weight;  //kg
    private final int chanceToBreak; // 1 if possible 0 if not
    private final String name;
    
//    public Item(){
//    }
    
    public Item(double s, double w, int c, String n) {
        size = s;
        weight = w;
        chanceToBreak = c;
        name = n;
    }

    public String getName() {
        return name;
    }
    
    public double getSize() {
        return size;
    }
    
    public double getWeight() {
        return weight;
    }

    public int getChanceToBreak() {
        return chanceToBreak;
    }
    
    // No need to call throwItem after item broken
    public boolean throwItem(TextArea textArea, double n) {
       // Test break chance
       boolean state = false;
       for(int i = 0; i<n; i++) {
            state = Math.random() < 0.001;
            if (state)
                break;
        }
       if (state == true) {
           textArea.setText("Esine hajosi.\n" + textArea.getText());
       }
       return state;
    }
    
    // To list predefined item names in combobox
    @Override
    public String toString() {
        return name;
    }
}
