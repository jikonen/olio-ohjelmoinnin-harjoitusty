/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;


/**
 *
 * @author Joonas
 */

class FirstClass extends Package {
    private final int maxDistance = 150; //km

    public FirstClass() {
        super(5000, 5, 100, "1. Luokka");
    }
    
    public int getMaxDistance() {
        return maxDistance;
    }

}

class SecondClass extends Package {    
    public SecondClass() {
        super(2000, 10, 0, "2. Luokka");
    }
}

class ThirdClass extends Package {
    private double throwFreq;
    
    public ThirdClass(double s, double w) {
        super(1000000, 500, (int)(-(s/1000000+w/500)*100/2+100), "3. Luokka"); 
                //Heavy and large dont break
                //And are thrown less often
                //Breakable 0-100 (hopefully)
        throwFreq = getBreakable(); // Have to try throwing atleast once
    }
    public double getThrowFreq() {
        return throwFreq;
    }
    
}


public abstract class Package {
    
    private final double maxSize;     //cm^3
    private final double maxWeight;   //kg
    private final int breakable;      // number of throws around 1-100
    private final String clas;      // Package class ("1. Luokka")
    private Item item;              // Item of the package
    private Smartpost start;        // Smartpost from which package is sent
    private Smartpost target;       // Target smartpost

   
    protected Package(double s, double w, int b, String c) {
        maxSize = s;
        maxWeight = w;
        breakable = b;
        clas = c;
    }
    
     public Item getItem() {
        return item;
    }

    public Smartpost getStart() {
        return start;
    }

    public Smartpost getTarget() {
        return target;
    }
    
    public double getMaxSize() {
        return maxSize;
    }

    public double getMaxWeight() {
        return maxWeight;
    }

    public int getBreakable() {
        return breakable;
    }
    
    public String getClas() {
        return clas;
    }
    
    // If creating package with user defined item, MUST GIVE NULL ITEM
    public void setItem(Item it, double s, double w, int b, String n) {
        if(it == null) {
            item = new Item(s, w, b, n);
        } else {
            item = it;
        }
    }
    public void setStart(Smartpost s) {
        start = s;
    }
    
    public void setTarget(Smartpost t) {
        target = t;
    }
    
    @Override
    public String toString() {
        return item.getName() + ", " + getClas();
    }
}