/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Joonas
 */
public class ParseXML {
    private ArrayList<Smartpost> array = new ArrayList<>();
    
    public ParseXML() {
        String content = getUrlContent();
        Document doc;
        try {
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            parsePlaceData(doc);
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(SpControl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Problem setting up parsing for XML");
        } catch (SAXException ex) {
            Logger.getLogger(SpControl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Problem parsing XML doc");
        } catch (IOException ex) {
            Logger.getLogger(SpControl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Problem reading XML content");
        }
        
    }
    public ArrayList<Smartpost> getSpArray() {
        return array;
    } 
    
    // Write content of xml into String content
    private String getUrlContent() {
        String con = "";
        String line;
        try {
            //Get smartpost info from xml
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(
                url.openStream()));
            while((line = br.readLine()) != null) {
                //System.out.println(line);
                con += line + "\n";
            }            

        } catch (MalformedURLException ex) {
            Logger.getLogger(SpControl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Smartpost URL faulty");
        } catch (IOException ex) {
            Logger.getLogger(SpControl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Problem reading Smartpost XML");
        }
        return con;
    }
    
    //Parse all info of a place to Smartpost class and add to array
    private void parsePlaceData(Document doc) {
        NodeList nodes = doc.getElementsByTagName("place");
        Smartpost sp;
        SpControl spc = SpControl.getInstance();
        for(int i = 0; i < nodes.getLength(); i++) {
            //System.out.println(nodes.getLength());
            Node node = nodes.item(i);
            Element e = (Element) node;
            //Create Smartpost object by info under place
            
            sp = new Smartpost(getValue("code", e), getValue("city", e),
                    getValue("address", e), getValue("availability", e),
                    getValue("postoffice", e), getValue("lat", e),
                    getValue("lng", e));
//            System.out.println(getValue("code", e));
//            System.out.println(getValue("lng", e));
            array.add(sp);
            //spArray.add(sp);
        }
    }
    
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
    
}
