/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;

import java.util.ArrayList;


/**
 *
 * @author Joonas
 */
public class SpControl {
    //Create singleton 
    private static class SpControlHolder {
        public static SpControl hsp = new SpControl(); 
    }
    
    private ArrayList<Smartpost> spArray = new ArrayList<>();
    
    public SpControl(){
        ParseXML pxml = new ParseXML();
        spArray = pxml.getSpArray();        
    }
    
    //Return object to create single SpControl
    public static SpControl getInstance(){
        return SpControlHolder.hsp;
    }
    
    // Get whole list, get sorted list by city or get smartpost by name
    public ArrayList<Smartpost> getSpArray(){
        return spArray;
    }
    
    // Returns array of smartposts filtered by city from whole sp list
    public ArrayList<Smartpost> getSpArrayOfCity(String loc) {
        ArrayList<Smartpost> spsOfCity = new ArrayList<>();
        for(Smartpost smp : spArray) {
            if((smp.getCity()).equals(loc)){
                spsOfCity.add(smp);
            }
        }
        return spsOfCity;
    }
    
    // Returns Smartpost from given array by location name
    public Smartpost getSpByName(String loc, ArrayList<Smartpost> al) {
        for(Smartpost sp : al) {
            if ((sp.getLocName()).equals(loc)) {
                return sp;
            }
        }
        return null;
    }
    
    public void addSp(Smartpost s) {
        spArray.add(s);
    }
}
