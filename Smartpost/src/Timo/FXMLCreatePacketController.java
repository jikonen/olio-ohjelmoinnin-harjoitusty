/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author Joonas
 */
public class FXMLCreatePacketController implements Initializable {
    @FXML
    private ComboBox<Smartpost> startSpCombo;
    @FXML
    private ComboBox<Smartpost> targetSpCombo;
    @FXML
    private RadioButton firstClassCheck;
    @FXML
    private RadioButton secondClassCheck;
    @FXML
    private RadioButton thirdClassCheck;
    @FXML
    private ComboBox<Item> itemCombo;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemSizeField;
    @FXML
    private TextField itemWeightField;
    @FXML
    private Button createPacketButton;
    @FXML
    private CheckBox breakableCheck;
    @FXML
    private TextField commentField;
    @FXML
    private ComboBox<String> startCityCombo;
    @FXML
    private ComboBox<String> targetCityCombo;
    @FXML
    private TextArea logTextArea;
    @FXML
    private Button refreshButton;
    @FXML
    private Label lastUpdated;
    
    final ToggleGroup classGroup = new ToggleGroup();
    private final Timo timo = new Timo();
    private final Storage store = timo.getStorage();
    private final SpControl spControl = timo.getSpControl();
    @FXML
    private Button emptyButton;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Combine class checks to group to avoid multiple checks
        firstClassCheck.setToggleGroup(classGroup);
        secondClassCheck.setToggleGroup(classGroup);
        thirdClassCheck.setToggleGroup(classGroup);
        
        // Fill item combo
        for(Item item : store.getItemList()) {
            itemCombo.getItems().add(item);
        }
        // Fill city combos with all cities
        ArrayList<Smartpost> ar = spControl.getSpArray();
        fillCityCombo(ar, startCityCombo);
        fillCityCombo(ar, targetCityCombo);
        
        // Add listeners and filtering to combos
        listenerStuff(ar, startCityCombo, startSpCombo);
        listenerStuff(ar, targetCityCombo, targetSpCombo);

    }    
    // Listening and filtering of combos
    private void listenerStuff(ArrayList<Smartpost> ar, ComboBox<String> cbc, 
            ComboBox<Smartpost> cbs) {
        cbc.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String city) {
                if (city.equals("Kaikki automaatit")) {
                    fillCombo(ar, cbs);
                } else {
                    cbs.getItems().clear();
                    for(Smartpost sp : spControl.getSpArrayOfCity(city)) {
                        cbs.getItems().add(sp);
                    }
                }
            }
        });
    }
    // Methods for filling packetcreation ComboBoxes
    private void fillCityCombo(ArrayList<Smartpost> ar, ComboBox<String> cb) {
        cb.getItems().add("Kaikki automaatit"); 
        for(Smartpost sp : ar) {
            if(!cb.getItems().contains(sp.getCity())) 
                cb.getItems().add(sp.getCity());
        }
    }
    private void fillCombo(ArrayList<Smartpost> ar, ComboBox<Smartpost> cb) {
        for(Smartpost sp : ar) {
            cb.getItems().add(sp);
        }
    }
    
    // Get packet class from class checks
    private int getPackageClass() {
        int cl = 1;
        for(Toggle tog : classGroup.getToggles()) {
            if(tog.isSelected()) {
                return cl;
            } else 
                cl++;
        }
        return cl;
    }
    

    @FXML
    private void createPacket(ActionEvent event) {
        // Get info for packet creation and choose between own and predefined
        //createPackage(Item it, double size, double weight, 
           // int breakChance, int cl, String name)
        commentField.setText("");
        int cl = getPackageClass();
        Double size = (double)0;
        Double weight = (double)0;
        int breakChance = 0;
        String name = "";
        String str = "";
        Smartpost start = startSpCombo.getValue();
        Smartpost target = targetSpCombo.getValue();
        // If no predefined selected get text field values and check for empty
        Item it = itemCombo.getValue();
        if (it == null) {
            try {
            size = Double.valueOf(itemSizeField.getText());
            weight = Double.valueOf(itemWeightField.getText());
            if (breakableCheck.selectedProperty().get()) {
                breakChance = 100;
            } else breakChance = 0;
            name = itemNameField.getText();
            str = store.checkItemFit(size, weight, cl);
            
            } catch(NumberFormatException ex) {
                commentField.setText("Täytä kentät oikein.");
            }
        } else {
            size = it.getSize();
            weight = it.getWeight();
            str = store.checkItemFit(size, weight, cl);
            
        }
        
        // Check for unexpected class number and non empty size check str
        if (cl == 4)
            commentField.setText("Valitse paketin luokka.");
        else if (!str.isEmpty())
            commentField.setText(str);
        else if (start == null || target == null)
            commentField.setText("Valitse alku ja loppukohteet lähetykselle");
        else {
            store.createPackage(it, size, weight, breakChance, cl, 
                    name, start, target);
        }
    }
    // Get latest entry and update amount of packages before writing textArea
    @FXML    
    private void refreshAction(ActionEvent event) {
        if (timo.getLog().getEntryAr() == null && store.getPackageList().size() == 0) {
            logTextArea.setText("Loki on tyhjä.");
        } else if (timo.getLog().getEntryAr() == null && store.getPackageList().size() != 0) {
            logTextArea.setText("Paketteja varastossa: " + store.getPackageList().size());
        } else {
            ArrayList<String> ar = timo.getLog().getEntryAr();
            ar.set(0, String.valueOf(store.getPackageList().size()));
            timo.getLog().updateLatest(ar);
            String time = timo.getLog().writeInfoTab(logTextArea);
            lastUpdated.setText(time);
        }
    }

    @FXML
    private void clearAllAction(ActionEvent event) {
        commentField.clear();
        itemWeightField.clear();
        itemSizeField.clear();
        itemNameField.clear();
        itemCombo.getItems().clear();
        // Fill item combo back up
        for(Item item : store.getItemList()) {
            itemCombo.getItems().add(item);
        }
        classGroup.getSelectedToggle().setSelected(false);
        
    }
}
