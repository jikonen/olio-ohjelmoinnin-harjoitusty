/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timo;

import java.util.ArrayList;

/**
 *
 * @author Joonas
 */
public class Storage {
    private static Storage storage = new Storage();
    private Package pac = null;
    private ArrayList<Package> boxArray;
    
    private Storage() {
        boxArray = new ArrayList<>();
    }
    
    public static Storage getInstance() {
        return storage;
    }
    public ArrayList<Package> getPackageList() {
        return boxArray;
    }
    public ArrayList<Item> getItemList() {
        ArrayList<Item> itemList = new ArrayList<>();
        itemList.add(new Letter());
        itemList.add(new Cake());
        itemList.add(new UnknownBrownMass());
        itemList.add(new Ied());
        itemList.add(new Anvil());
        return itemList;
    }
    
    // Return empty string if OK, else string for textfield
    public String checkItemFit(Double itSize, Double itWeight, int cl) {
        String str = "";
        String str1 = "Liian suuri esine valittuun luokkaan";
        String str2 = "Liian painava esine valittuun luokkaan";
        if (cl == 1) {Package pk = new FirstClass();
            if (itSize > pk.getMaxSize()) {
                str = str1;
            }
            if (itWeight > pk.getMaxWeight()) {
                str = str2;
            }
        } else if (cl == 2) {Package pk = new SecondClass();
            if (itSize > pk.getMaxSize()) {
                str = str1;
            }
            if (itWeight> pk.getMaxWeight()) {
                str = str2;
            }
        } else if (cl == 3) {Package pk = new ThirdClass(itSize, itWeight);
            if (itSize> pk.getMaxSize()) {
                str = str1;
            }
            if (itWeight> pk.getMaxWeight()) {
                str = str2;
            }
        } else
            System.err.println("Unpredicted class selection number in checkItemFit");
        
        return str;
    }
   
    public void createPackage(Item it, double size, double weight, 
            int breakChance, int cl, String name, Smartpost start,
            Smartpost target) {
        
        switch(cl) {
            case 1:
                //System.out.println("1st class creation");
                //Class 1 Package
                pac = new FirstClass();
                pac.setItem(it, size, weight, breakChance, name);
                pac.setStart(start);
                pac.setTarget(target);
                break;
            case 2:
                //Class 2 Package
                //System.out.println("2st class creation");
                pac = new SecondClass();
                pac.setItem(it, size, weight, breakChance, name);
                pac.setStart(start);
                pac.setTarget(target);
                break;
            case 3:
                //Class 3 Package
                //System.out.println("3st class creation");
                
                if (it != null){
                    //Prevent giving ThirdClass empty size and weight
                    size = it.getSize();
                    weight = it.getWeight();
                }
                pac = new ThirdClass(size, weight);
                pac.setItem(it, size, weight, breakChance, name);
                pac.setStart(start);
                pac.setTarget(target);
                break;
            default:
                System.err.println("Error in class number, "
                        + "check class number in createPackage call");
                
        }
        boxArray.add(pac);
    }
}
